<?php
session_start();
function isAuthorised(){
    if (!empty($_SESSION['user']) && !empty($_SESSION['userId'])){
        return true;
    }else{
        return false;
    }
}

function isPost(){
    return $_SERVER['REQUEST_METHOD'] == 'POST';
}

function getParamPost($name){
    return isset($_POST[$name]) ? $_POST[$name] : null;
}

function logout(){
    session_destroy();
}
function special($str){
    $str = htmlspecialchars($str);
    return $str;
  }
?>