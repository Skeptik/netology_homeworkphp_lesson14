<?php
require_once __DIR__.'/core/function.php';
if (!isAuthorised()) {
    echo "<a href='register.php'>Войдите на сайт</a>";
    die;
}
require_once __DIR__.'/core/connectBD.php';
$flagEdit = false;
$str = '';
$id = 0;
if(!empty($_GET['id']) && !empty($_GET['action'])){
    switch ($_GET['action']) {
        case 'edit':
        $flagEdit = true;
        $id = $_GET['id'];
        $results = $db->query('SELECT description FROM task WHERE id='.$id);
        while ($row = $results->fetch(PDO::FETCH_ASSOC)){
            $str = $row['description'];
        }
        break;
        case 'done':
        $check = 'UPDATE task SET is_done = 1 WHERE id='.$_GET['id'];
        break;
        case 'delete':
        $check = 'DELETE FROM task WHERE id ='.$_GET['id'];
        break;
    }
    if(!$flagEdit){
        $db->query($check);
    }
}
if(!empty($_POST['description']) && $_POST['save'] =='Сохранить'){
    $spec = special($_POST['description']);
    $check = "UPDATE task SET description ='".$spec."' WHERE id=".$id;
    $flagEdit = false;
    $db->query($check);
}
if(!empty($_POST['assigned_user_id'])){
    $data = explode('-', $_POST['assigned_user_id']);
    foreach ($data as $value){
        $v = explode('_', $value);
        $asocArr[$v[0]] = $v[1];
    }
    $results = $db->query("UPDATE task SET assigned_user_id=".$asocArr['user']." WHERE id=".$asocArr['task']);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
    table {
        border-spacing: 0;
        border-collapse: collapse;
        font-size: 18px;
    }

    table td, table th {
        border: 1px solid #ccc;
        padding: 5px;
    }

    table th {
        background: #eee;
    }
    div {
        padding: 7px;
        padding-right: 20px;
        border: solid 1px black;
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 13pt;
        background: #E6E6FA;
       }
</style>
</head>
<body>

<h1>Здравствуйте, <?= $_SESSION['user'] ?>! Вот ваш список дел:</h1>
<div style="float: left">
    <?php
    if(!$flagEdit){
    ?>
    <form method="POST" action="#" name="myform">
        <input type="text" name="description" placeholder="Описание задачи" value="" />
        <input type="submit" name="save" value="Добавить" />
    </form>
    <?php
    }else{
    ?>
    <form method="POST" action="#" name="myform">
        <input type="text" name="description" placeholder="Описание задачи" value="<?= $str ?>" />
        <input type="submit" name="save" value="Сохранить" />
    </form>
    <?php
    }
    ?>
</div>
<?php
    try{
        if (!empty($_POST['description']) && $_POST['save'] =='Добавить'){
            $spec = special($_POST['description']);
            $check = "INSERT INTO task (description, date_added, user_id, assigned_user_id)
            VALUES ('".$spec."', '".date("Y-m-d H:i:s")."', '".$_SESSION['userId']."', '".$_SESSION['userId']."')";
            $db->query($check);
        }
        $results = $db->query("
            SELECT t.id, t.description, t.date_added, t.is_done, u2.login assigned_name, u1.login user_name
            FROM task t
            JOIN user u1 ON u1.id=t.user_id
            JOIN user u2 ON u2.id=t.assigned_user_id
            WHERE t.user_id =".$_SESSION['userId']
            );
        $error_array = $db->errorInfo();
        if($db->errorCode() != 0000){
            echo "SQL ошибка ".$error_array[2].'<br>';
            die("Error");
        }
    } catch (PDOException $e){
        die("Error: ".$e->getMessage());
    }
?>
    <table>
      <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
        <th>Ответственный</th>
        <th>Автор</th>
        <th>Закрепить задачу за пользователем</th>
    </tr>
    <br><br><br>
<?php
    while ($row = $results->fetch(PDO::FETCH_ASSOC)){
?>
     <tr>
        <td><?= $row['description'] ?></td>
        <td><?= $row['date_added'] ?></td>
        <td><?= ($row['is_done'] == 0) ? "<span style='color: orange'>В процессе</span>" :
             "<span style='color: green'>Выполнено</span>" ?>
        </td>
        <td>
            <a href='?id=<?= $row['id'] ?>&action=edit'>Изменить</a>
            <a href='?id=<?= $row['id'] ?>&action=done'>Выполнить</a>
            <a href='?id=<?= $row['id'] ?>&action=delete'>Удалить</a>
        </td>
        <td><?= $row['assigned_name'] ?></td>
        <td><?= $row['user_name'] ?></td>
        <td>
            <form method='POST'>
                <select name='assigned_user_id'>
                <?php
                $re = $db->query("SELECT id, login FROM user");
                while ($ro = $re->fetch(PDO::FETCH_ASSOC)){
                 ?>
                    <option value="user_<?= $ro['id'] ?>-task_<?= $row['id'] ?>"><?= $ro['login'] ?></option>
                <?php
                }
                ?>
                </select>
            <input type='submit' name='assign' value='Переложить ответственность' />
            </form>
        </td>
        </tr>
<?php
    }
?>
</table>
<h3>Также, посмотрите, что от Вас требуют другие люди:</h3>
<?php
    try{
        $results = $db->query("
            SELECT t.id, t.description, t.date_added, t.is_done, u2.login assigned_name, u1.login user_name
            FROM task t
            JOIN user u1 ON u1.id=t.user_id
            JOIN user u2 ON u2.id=t.assigned_user_id
            WHERE t.assigned_user_id =".$_SESSION['userId']
            );
        $error_array = $db->errorInfo();
        if($db->errorCode() != 0000){
            echo "SQL ошибка ".$error_array[2].'<br>';
            die("Error");
        }
    } catch (PDOException $e){
        die("Error: ".$e->getMessage());
    }
?>
    <table>
      <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
        <th>Ответственный</th>
        <th>Автор</th>
    </tr>
    <br><br><br>
<?php
    while ($row = $results->fetch(PDO::FETCH_ASSOC)){
        if($row['user_name'] == $_SESSION['user']){
            continue;
        }
?>
     <tr>
        <td><?= $row['description'] ?></td>
        <td><?= $row['date_added'] ?></td>
        <td><?= ($row['is_done'] == 0) ? "<span style='color: orange'>В процессе</span>" :
             "<span style='color: green'>Выполнено</span>" ?>
        </td>
        <td>
            <a href='?id=<?= $row['id'] ?>&action=done'>Выполнить</a>
        </td>
        <td><?= $row['assigned_name'] ?></td>
        <td><?= $row['user_name'] ?></td>
        </tr>
<?php
    }
?>
</table>
<h3><a href='logout.php'>Выход</a></h3>
</body>
</html>